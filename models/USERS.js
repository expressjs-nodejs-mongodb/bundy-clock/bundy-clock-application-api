const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    fName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
    cntct_num: {
		type: Number,
		required: [true, "Number is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	date_reg:{
		type: String,
		required: [true, "Time is required!"]	
	},
	// timelogs:{
	// 	type: String,
	// }
});
module.exports = mongoose.model("USER", userSchema);