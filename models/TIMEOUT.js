const mongoose = require("mongoose");


const timeoutSchema = new mongoose.Schema({
    userId:{
		type: String,
        required: [true, "Time is required!"]
	},
    email:{
		type: String,
        required: [true, "Time is required!"]
	},
    time_out:{
        type: String,
        required: [true, "Time is required!"]
    },
    date_log:{
        type: String,
        required: [true, "Time is required!"]	
    },
    time_logs_confirm:{
        type: String,
        required: [true, "Successfully"]
    }

});
module.exports = mongoose.model("TIMEOUT", timeoutSchema);