// *****
const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const timeioControllers = require("../controllers/timeiocontrollers.js");



// Time In
router.post("/time_in", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }
	timeioControllers.time_in(data).then(resultFromController => res.send(resultFromController));
});
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RETRIEVE USER TIME DETAILS

router.get("/retrieve", (req, res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,}
        timeioControllers.getUserTime(data).then(resultFromController => res.send(resultFromController));
});


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ADMIN RETRIEVE USER TIME DETAILS

// router.get("/adminretrieve", (req, res) => {
router.get("/:email", (req, res) => {
    const data = {
        reqParams: req.params,
        user: auth.decode(req.headers.authorization),
        order: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin}
        timeioControllers.getUserTimeadmin(data).then(resultFromController => res.send(resultFromController));
});

/*==================================================================================================*/
module.exports = router;