// *****
const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const timeoutControllers = require("../controllers/timeoutcontrollers.js");

// TIME OUT
router.post("/time_out", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }
    timeoutControllers.time_out(data).then(resultFromController => res.send(resultFromController));
});
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RETRIEVE USER TIME DETAILS

router.get("/retrieve", (req, res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,}
        timeoutControllers.getUserTimeOut(data).then(resultFromController => res.send(resultFromController));
});


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ADMIN RETRIEVE USER TIME DETAILS

router.get("/:email", (req, res) => {
    const data = {
        reqParams: req.params,
        user: auth.decode(req.headers.authorization),
        order: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin}
        timeoutControllers.getUserTimeadmin(data).then(resultFromController => res.send(resultFromController));
});

/*==================================================================================================*/
module.exports = router;