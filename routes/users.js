// *****
const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userControllers = require("../controllers/usercontrollers.js");

/*==================================================================================================*/
// Check email if it is existing to our database
router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// User Registration route
router.post("/register", (req, res) => {
	userControllers.regUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User LogIn
router.post("/logIn", (req, res) => {
	userControllers.logIn(req.body).then(resultFromController => res.send(resultFromController));
});


// Time Out
router.post("/time_out", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }
	userControllers.time_out(data).then(resultFromController => res.send(resultFromController));
});



// ADMIN RETRIEVE ALL USER DETAILS
router.get("/retrieveall", (req, res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin}
	userControllers.getAllUserInfo(data).then(resultFromController => res.send(resultFromController));
});

/*==================================================================================================*/
module.exports = router;