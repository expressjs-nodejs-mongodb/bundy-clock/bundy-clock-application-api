const USER = require("../models/USERS.js");
const TIMEIO = require("../models/TIMEIO.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// TIME IN
module.exports.time_in = async (data) => {

    const time_logs_confirm = "Succesfull"
    let time_in = new Date();
    

    const date_log = new Date();

    const user = await USER.findOne({email:data.user.email});

    let fname = user.fName
    let lname = user.lName
    const fullname = fname + " " + lname

            let timein = TIMEIO({

            userId: fullname,
            email: user.email,

            time_logs_confirm: time_logs_confirm,
                                //time_in adding location time -> to recognize what timezone
            date_log: (date_log[Symbol.toPrimitive]('string')),
            time_in: time_in.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true }) 
    })
    // user.timelogs.push(newtime);
    console.log(timein);

    return await timein.save().then((save, err) =>{
        if(err){
            return false;
        }else{
            return true;
        }
    })
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 // RETRIEVE SINGLE USESER TIME DETAILS -- USERS
 module.exports.getUserTime = async (data) => {
    // const user = await USER.findOne({email:data.user.email});
	return TIMEIO.find({email:data.user.email}).then(result => {
        console.log(result)

        return result
	});
    
};


                                        // RETRIEVE USER -- DETAILS ADMIN ONLY 
// Allow the ADMIN to access user personal information including time in/out 
module.exports.getUserTimeadmin = async(data) => { 
    if(data.isAdmin){
    return TIMEIO.find({email:data.reqParams.email}).then(result => {
        console.log(result)

        return result
	});

        }else{
        return false;
    }

};