const USER = require("../models/USERS.js");
const TIMEOUT = require("../models/TIMEOUT.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// TIME OUT
module.exports.time_out = async (data) => {

    const time_logs_confirm = "Succesfull"
    let time_out = new Date();
    

    const date_log = new Date();

    const user = await USER.findOne({email:data.user.email});

    let fname = user.fName
    let lname = user.lName
    const fullname = fname + " " + lname

            let timeout = TIMEOUT({

            userId: fullname,
            email: user.email,

            time_logs_confirm: time_logs_confirm,
                                //time_in adding location time -> to recognize what timezone
            date_log: (date_log[Symbol.toPrimitive]('string')),
            time_out: time_out.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true }) 
    })

    return await timeout.save().then((save, err) =>{
        if(err){
            return false;
        }else{
            return true;  
        }

    })
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 // RETRIEVE SINGLE USESER TIME DETAILS -- USERS
 module.exports.getUserTimeOut = async (data) => {
    // const user = await USER.findOne({email:data.user.email});
	return TIMEOUT.find({email:data.user.email}).then(result => {
        console.log(result)

        return result
	});
    
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                       // RETRIEVE USER -- DETAILS ADMIN ONLY 
// Allow the ADMIN to access user personal information including time in/out 
module.exports.getUserTimeadmin = async(data) => { 
    if(data.isAdmin){
    return TIMEOUT.find({email:data.reqParams.email}).then(result => {
        console.log(result)

        return result
	});

        }else{
        return false;
    }

};