const USER = require("../models/USERS.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// code blocks all needed action put here
/*==================================================================================================*/
module.exports.checkEmailExists = (reqBody) => {
	return USER.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}




//User Registration 
module.exports.regUser = async(reqBody) =>{

	const date = new Date();

	let newUser = new USER({
		fName: reqBody.fName,
		lName: reqBody.lName,
		email: reqBody.email,
        cntct_num: reqBody.cntct_num,
		password: bcrypt.hashSync(reqBody.password, 8),

		date_reg: (date[Symbol.toPrimitive]('string'))
	});
	return await newUser.save().then((save,err) =>{
		if(err){
			return false;
		}else{
			return true;
		}
	})
};


                                                 // User LogIn
module.exports.logIn = async (reqBody) => {
	return USER.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
                console.log(result)
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
};



                                        // RETRIEVE ALL USER -- DETAILS ADMIN ONLY 
// Allow the ADMIN to access all user personal information including time in/out 
module.exports.getAllUserInfo = async(data) => { 
    
	if(data.isAdmin){
            return await USER.find({}).then(result => {
                return result;
            });
 }else{
 	return false;
 }
};






